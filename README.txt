A Simple Client demonstrating endeca serarch.

Steps to run the client:
-----------------------------------------------------------------------------------------------------------------------
1. mvn jetty:run 
2. Access the search page at http://localhost:9090/endeca-client-poc

Sample Search URLS: 
- http://localhost:9090/endeca-client-poc/views/search?N=0&Ne=2&Ntk=P_Description&Ntt=mirror
- http://localhost:9090/endeca-client-poc/views/search?N=0

Environment specific properties
-----------------------------------------------------------------------------------------------------------------------
Environment specific property files are stored in
- For Dev environment: src/main/config/DEV
- For Test environment: src/main/config/TEST
- For Production environment: src/main/config/PROD

Steps to build the war file
-----------------------------------------------------------------------------------------------------------------------
- For dev deployments use: mvn install -P dev
- For test deployments use: mvn install -P test
- For production deployments use: mvn install -P prod

ENDECA Notes:
-----------------------------------------------------------------------------------------------------------------------
Product:
 - Oracle provides Endeca as part 1) Oracle Commerce suite and 2) Oracle Endeca Information Discovery(OEID) 
 - Endeca search engine is termed MDEX
 - Guided navigation and search
 - Business Intelligence

API:
1. Endeca record: A denormalized data record with properties
2. Endeca record properties: A record is composed of key/value pairs that store record property value against keys. Leaf Nodes of a search tree
3. Endeca Dimension: Logical structure to records. Search facet. Group by clause. Classification on record set. Non-Leaf Node of a search tree
4. Endeca nid: Dimension ID, nid=0 means root dimension and a search on it will span the entier record set available.
5. Searchable vs Non-Searchable: Indexed properties are considered searchable and non-indexed properties are non-searchable.
6. Dimension Group: A group of dimensions. Defines relations among dimensions. One dimension can belong to only one dimension group. product.category, product_features=> camera.color-> red, blue, green. A group of non-leaf nodes of a search tree. getNavigation().getCompleteDimGroups();
7. Search Types: Navigation Search and Keyword Search
7.1 Navigation Search: Search by navigation dimension id using ENEQuery::setNavDescriptors(dimValIdList);
7.2 Keyword Search: Search for keywords in either the dimensions or the records
7.2.1 Keyword Search on records-> ENEQuery :: setNavERecSearches(ERecSearchList)
7.2.2 Keyword Search on dimensions-> query.setDimSearchTerms(<search term>)
8. Guided navigation: Displaying dimensions and their values for query refinement 



Managing external non-maven dependencies 
-----------------------------------------------------------------------------------------------------------------------
This project uses a embedded maven repository to handle external dependencies that are not hosted on maven repository. 
<!-- deploy jar files to project local maven repo to ease distribution -->
<!-- for more information refer to this blog: https://devcenter.heroku.com/articles/local-maven-dependencies -->
<!-- ***NOTE: current source has this repo setup under endeca-client-poc/maven/repo directory. Use these commands only while updating the libraries -->
mvn deploy:deploy-file -Durl=file:///c:/projects/endeca-client-poc/maven/repo/ -Dfile=endeca_logging.jar -DgroupId=com.endeca -DartifactId=endeca-logging -Dpackaging=jar -Dversion=1.0
mvn deploy:deploy-file -Durl=file:///c:/projects/endeca-client-poc/maven/repo/ -Dfile=endeca_navigation.jar -DgroupId=com.endeca -DartifactId=endeca-navigation -Dpackaging=jar -Dversion=1.0
-----------------------------------------------------------------------------------------------------------------------